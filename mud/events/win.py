# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event1

class WinEvent(Event1):
    NAME = "win"

    def perform(self):
        self.inform("win")
